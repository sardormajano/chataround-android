import React, {Component} from 'react';
import {Container, Content, Text} from 'native-base';

export default class TabTwo extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Text>
            Это содержание местопложения
          </Text>
        </Content>
      </Container>
    );
  }
}
