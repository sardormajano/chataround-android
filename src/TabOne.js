import React, {Component} from 'react';
import {Image} from 'react-native';
import {
  Container, Content, Text,
  Card, CardItem, Button, Thumbnail
} from 'native-base';

import {styles} from './styles/styles.js';

export default class TabOne extends Component {
  render() {
    return (
      <Container style={styles.container} >
        <Content>
          <Card style={styles.scanCard}>
            <CardItem>
              <Thumbnail source={{uri: 'https://cdn3.iconfinder.com/data/icons/logistics-delivery-set-2/512/9-512.png'}} />
              <Text>Отсканируйте qr-код</Text>
            </CardItem>
            <CardItem>
              <Image style={{ resizeMode: 'stretch' }} style={styles.scanImage} source={{uri: 'http://i.stack.imgur.com/FHvNV.png'}} />
            </CardItem>
            <CardItem style={styles.scanButtonParent}>
              <Button success onPress={this.props.scanButtonHandler}>Сканировать</Button>
            </CardItem>
           </Card>
        </Content>
      </Container>
    );
  }
}
