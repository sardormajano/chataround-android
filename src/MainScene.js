import React, {Component} from 'react';
import {Container, Content, Tabs, Text, Footer, FooterTab, Icon, Button} from 'native-base';
import {styles} from './styles/styles.js';
import customTheme from './Themes/customTheme.js'

import ScrollableTabView, {DefaultTabBar, } from 'react-native-scrollable-tab-view';

import TabOne from './TabOne.js';
import TabTwo from './TabTwo.js';

export default class MainScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: 0
    }
  }
  render() {
    let CurrentTab = undefined;

    if(this.state.currentTab === 0)
      CurrentTab = TabOne;
    else if(this.state.currentTab === 1)
      CurrentTab = TabTwo;

    return (
      <Container theme={customTheme}>
          <Content>
            <CurrentTab scanButtonHandler={this.props.scanButtonHandler} />
          </Content>
          <Footer >
            <FooterTab>
              <Button active={this.state.currentTab === 0} onPress={() => this.setState({currentTab: 0})}>
                  Сканирование
                  <Icon name='ios-camera-outline' />
              </Button>
              <Button active={this.state.currentTab === 1} onPress={() => this.setState({currentTab: 1})}>
                  Местоположение
                  <Icon name='ios-map' />
              </Button>
            </FooterTab>
          </Footer>
      </Container>
    );
  }
}
