import React, {Component} from 'react';
import BarcodeScanner from 'react-native-barcodescanner';

export default class ScannerScene extends Component {
  constructor(props) {
    super(props);

    this.state = {
      torchMode: 'off',
      cameraType: 'back'
    }
  }

  render() {
    return (
      <BarcodeScanner
        onBarCodeRead={this.props.barcodeReceived}
        style={{ flex: 1 }}
        torchMode={this.state.torchMode}
        cameraType={this.state.cameraType}
      />
    );
  }
}
