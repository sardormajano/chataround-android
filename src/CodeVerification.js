import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container, Content, Header, Footer, FooterTab,
  Title, Button, Icon, Card, CardItem, InputGroup,
  Input, List, ListItem, Radio, Text
} from 'native-base';

import {styles} from './styles/styles.js'

export default class LoginScene extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedSex: 'male',
    };
  }

  setSelectedSex(selectedSex) {
    this.setState({
      selectedSex
    });
  }

  render() {
    return (
      <Container>
        <Header>
          <Title>Введите код верификации</Title>
        </Header>

        <Content>
            <Card>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input defaultValue="+77783524928"/>
                </InputGroup>
              </CardItem>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input placeholder='Введите код верификации'/>
                </InputGroup>
              </CardItem>
           </Card>
        </Content>

        <Footer>
          <FooterTab>
            <Button transparent onPress={this.props.verifyButtonHandler}>
                <Icon style={{fontSize: 30, fontWeight: 'bold'}} name='ios-log-in' />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
   )
  }
}
