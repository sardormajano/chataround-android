import { StyleSheet, Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  scanImage: {
    width: parseInt(width * 0.5),
    height: parseInt(height * 0.3)
  },
  scanCard: {
    marginTop: parseInt(height * 0.2)
  },
  scanButtonParent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
