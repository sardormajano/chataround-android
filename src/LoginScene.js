import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container, Content, Footer, FooterTab, Header,
  Title, Button, Icon, Card, CardItem, InputGroup,
  Input, List, ListItem, Radio, Text
} from 'native-base';

import {styles} from './styles/styles.js'

export default class LoginScene extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedSex: 'male',
    };
  }

  setSelectedSex(selectedSex) {
    this.setState({
      selectedSex
    });
  }

  render() {
    return (
      <Container>
        <Header>
          <Title>Добро пожаловать в Chataround</Title>
        </Header>

        <Content>
            <Card>
              <CardItem>
                <Image style={{ flex: 1, resizeMode: 'stretch' }} source={{uri: 'https://d2gg9evh47fn9z.cloudfront.net/thumb_COLOURBOX1239134.jpg'}} />
              </CardItem>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input placeholder='Введите свое имя'/>
                </InputGroup>
              </CardItem>
              <CardItem>
                <List>
                  <ListItem onPress={() => this.setState({selectedSex: 'male'})}>
                    <Radio
                      selected={this.state.selectedSex === 'male'}
                      onPress={() => this.setState({selectedSex: 'male'})}/>
                    <Text>Муж</Text>
                  </ListItem>
                  <ListItem onPress={() => this.setState({selectedSex: 'female'})}>
                    <Radio
                      selected={this.state.selectedSex === 'female'}
                      onPress={() => this.setState({selectedSex: 'female'})}/>
                    <Text>Жен</Text>
                  </ListItem>
                </List>
              </CardItem>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input placeholder='Введите свой номер телефона'/>
                </InputGroup>
              </CardItem>
           </Card>
        </Content>

        <Footer>
          <FooterTab>
            <Button transparent onPress={this.props.loginButtonHandler}>
                <Icon style={{fontSize: 30, fontWeight: 'bold'}} name='ios-log-in' />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
   )
  }
}
