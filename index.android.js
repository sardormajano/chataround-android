/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry, StyleSheet, Text, View,
  Image, TextInput, TouchableHighlight,
  Navigator, BackAndroid
} from 'react-native';
import {styles} from './src/styles/styles.js';

/* BEGIN SCENES */
import LoginScene from './src/LoginScene.js';
import MainScene from './src/MainScene.js';
import ScannerScene from './src/ScannerScene.js';
import CodeVerification from './src/CodeVerification.js';
/* END SCENES */

class hsBrowser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      qrcode: undefined
    }
  }

  loginButtonHandler(navigator) {
    navigator.push({
      title: 'CodeVerification'
    });
  }

  verifyButtonHandler(navigator) {
    navigator.push({
      title: 'MainScene'
    });
  }

  scanButtonHandler(navigator) {
    navigator.push({
      title: 'ScannerScene',
    });
  }

  scanSuccessHandler(navigator) {
    navigator.pop();
  }

  barcodeReceived(navigator) {
    navigator.pop();
  }

  render() {
    return (
      <Navigator
          initialRoute={{ title: 'LoginScene'}}
          renderScene={(route, navigator) => {
            BackAndroid.addEventListener('hardwareBackPress', (e) => {
              navigator.pop();
              return true;
            });

            switch(route.title) {
              case 'LoginScene':
                return <LoginScene loginButtonHandler={this.loginButtonHandler.bind(this, navigator)}/>;
              case 'MainScene':
                return <MainScene scanButtonHandler={this.scanButtonHandler.bind(this, navigator)}/>;
              case 'CodeVerification':
                return <CodeVerification verifyButtonHandler={this.verifyButtonHandler.bind(this, navigator)}/>;
              case 'ScannerScene':
                return <ScannerScene barcodeReceived={this.barcodeReceived.bind(this, navigator)}/>;
            }
          }}/>
    );
  }
}


AppRegistry.registerComponent('hsBrowser', () => hsBrowser);
